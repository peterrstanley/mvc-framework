<?php
/**
 * Menu Controller Class
 * 
 * This class is the menu Controller class that is used to handle all the page calls
 * for managing the menu module.
 * 
 * @package Menu
 * @author Peter R Stanley <peterrstanley@gmail.com>
 * @version v0.1.0
 * @copyright (c) 2013, Peter R Stanley
 * 
 */
class Menu_Controller_Index extends Controller {

	public function __construct(){
		parent::__construct();
		$this->view->module = 'Menu';
		$this->view->controller = $this;
	}

	public function Index(){
		$this->view->model = $this->model->getModel('menu/menu');
		$this->view->render('menu/index');
	}

	public function menuEdit($menuid){
		$this->view->menu_id = ($menuid === "") ? 'new' : strtolower($menuid);
		$this->view->model = $this->model->getModel('menu/menu');
		$this->view->render('menu/edit');
	}

	public function menuDelete($menuid){
		$menu = $this->model->getModel('menu/menu');
		if($menu->deleteMenu($menuid) === true){
			$this->ErrorHandler->addError('Menu has been successfully deleted!','success');
			$this->redirectPath('/menu');
		} else {
			$this->ErrorHandler->addError('Menu was not deleted!','warning');
			$this->redirectPath('/menu');
		}
	}

	public function menuEditPost(){
		$menu = $this->model->getModel('menu/menu');
		if(strtolower($_POST['menu_id']) === 'new') {
			unset($_POST['menu_id']);
			$res = $menu->createMenu($_POST);
			if(!is_numeric($res)){
				$this->ErrorHandler->addError('Error when creating menu');
				$this->redirectPath('/menu');
			} else {
					$this->ErrorHandler->addError('Menu successfully added!','success');
					$this->redirectPath('/menu/menuEdit/'.$res);
			}
		} else {
			$res = $menu->updateMenu($_POST['menu_id'],$_POST);
			if($res === true){
				$this->ErrorHandler->addError('Error when updating the menu.');
				$this->redirectPath('/menu/menuEdit/'.$_POST['menu_id']);
			} else {
					$this->ErrorHandler->addError('Menu successfully updated!','success');
					$this->redirectPath('/menu/menuEdit/'.$_POST['menu_id']);
			}
		}
	}
	
	public function items($menu_id){
		$this->view->menu_id = $menu_id;
		$this->view->render('menu/item/index');
	}

	public function itemEdit($item_id){
		$this->view->item_id = $item_id;
		$this->view->menu_id = $this->args[1];
		$this->view->model = $this->model->getModel('menu/item');
		$this->view->render('menu/item/edit');
	}

	public function itemEditPost(){
		if($_POST['item_id'] === 'new'){
			echo 'insert';
			$item = $this->model->getModel('menu/item');
			$res = $item->createItem($_POST['menu_id'],$_POST);
			if($res === false)
				$this->ErrorHandler->addError('Unable to create menu item!');
			else			
				$this->ErrorHandler->addError('Menu item was successfully created!','success');

		} else {
			$item = $this->model->getModel('menu/item');
			$res = $item->updateItem($_POST['item_id'],$_POST);
			if($res === false)
				$this->ErrorHandler->addError('Unable to update menu item!');
			else			
				$this->ErrorHandler->addError('Menu item was successfully updated!','success');
		}

		$this->redirectPath('/menu/menuEdit/'.$_POST['menu_id']);
	}

	public function itemDelete($item_id){
		$item = $this->model->getModel('menu/item');
		$res = $item->deleteItem($item_id);
		if($res === false)
			$this->ErrorHandler->addError('Unable to delete menu item.');
		
		$this->redirectPath('/menu');
	}

	public function generateMenu($menu_id){
		$menu = $this->model->getModel('menu/menu');
		$orien = $menu->getMenuAttr($menu_id,'orientation');
		$_view = new View();
		//$this->view->menu_id = $menu_id;
		//$this->view->model = $this->model->getModel('menu/item');
		$_view->controller = $this;
		$_view->menu_id = $menu_id;
		$_view->model = $this->model->getModel('menu/item');
		$_view->title = $_view->model->getItemLabel($this->getPath(true));
		switch ($orien) {
			case 'vertical' :
				//$this->view->renderBlock('menu/vertical');
				$_view->renderBlock('menu/vertical');
				break;
			case 'navbar' :
				//$this->view->renderBlock('menu/vertical');
				$_view->renderBlock('menu/navbar');
				break;
			default:
				echo $orien;
				//$this->view->renderBlock('menu/horizontal');
				$_view->renderBlock('menu/horizontal');
				break;
		}
	}
}
