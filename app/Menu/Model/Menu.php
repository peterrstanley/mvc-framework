<?php

class Menu_Model_Menu extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function createMenu($data){		
		try {
			$ith = $this->db->prepare(
				"INSERT INTO menus (menu_name,orientation) VALUES(:name,:orientation)");			
			$ith->execute(array(':name'=>$data['menu_name'],':orientation'=>$data['orientation']));
			return $this->db->lastInsertId();
		} catch (PDOExeption $e) {
			$this->ErrorHandler->addError($e);			
			$this->controller->redirectPath('/menu');
		}
	}
	
	public function updateMenu($menu_id,$data){
		unset($data['menu_id']);
		$data_arr[':menuid'] = $menu_id;
		foreach($data as $key=>$value){
			$data_arr[':'.$key] = $value;
			$sql_arr[] = $key.'=:'.$key;
		}
		$uth = $this->db->prepare("UPDATE menus SET ".implode(',',$sql_arr).' WHERE menu_id=:menuid');
		$uth->execute($data_arr);
	}

	public function deleteMenu($menu_id){
		try {
			$dth = $this->db->prepare("DELETE FROM menus WHERE menu_id=:menuid");
			$dth->execute(array(':menuid'=>$menu_id));
		} catch (PDOExeption $e) {
			echo $e;
			return false;
		}
		return true;
	}

	public function getMenu($menu_id){
		$sth = $this->db->prepare("SELECT * FROM menus WHERE menu_id=:menuid");
		$sth->execute(array(':menuid'=>$menu_id));
		return $sth->fetch();
	}

	public function getMenuAttr($menu_id,$attr){

		$sth = $this->db->prepare("SELECT ".$attr." FROM menus WHERE menu_id=:menuid");

		$sth->execute(array(':menuid'=>$menu_id));
		$temp = $sth->fetch();
		return $temp[$attr];
	}

	public function getMenus(){
		$sth = $this->db->prepare("SELECT * FROM menus");
		$sth->execute();
		return $sth->fetchAll(PDO::FETCH_ASSOC);
	}
}
