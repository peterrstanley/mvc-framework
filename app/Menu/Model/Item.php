<?php

class Menu_Model_Item extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function createItem($menu_id,$data){
		ini_set('display_errors',1);
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		try {
			$ith = $this->db->prepare("INSERT INTO menu_items (menu_id,label,path,`order`) VALUES (:menuid,:label,:path,:order)");
			$ith->execute(array(
				':menuid'=>$menu_id,			
				':label'=>$data['label'],
				':path'=>$data['path'],
				':order'=>$data['order']
			));
			return true;
		} catch (PDOExeption $e) {
			return $e;
		}
	}

	public function updateItem($item_id,$data){
		try {
			$uth = $this->db->prepare("UPDATE menu_items SET path=:path,label=:label,`order`=:order WHERE item_id=:itemid");
			$uth->execute(array(
				':itemid'=>$item_id,			
				':label'=>$data['label'],
				':path'=>$data['path'],
				':order'=>$data['order']
			));
			return true;
		} catch (PDOExeption $e) {
			return $e;

		}
	}

	public function deleteItem($item_id){
		try {
			echo $item_id;
			$dth = $this->db->prepare("DELETE FROM menu_items WHERE item_id=:itemid");
			$dth->execute(array(':itemid'=>$item_id));
			return true;
		} catch (PDOExeption $e) {
			return $e;
		}
	}

	public function getItem($item_id){
		try {
			$sth = $this->db->prepare("SELECT * FROM menu_items WHERE item_id=:itemid");
			$sth->execute(array(':itemid'=>$item_id));
			return $sth->fetch();
		} catch (PDOExeption $e) {
			return $e;
		}
	}

	public function getItems($menu_id){
		try {
			$sth = $this->db->prepare("SELECT * FROM menu_items WHERE menu_id=:menuid ORDER BY `order` ASC");
			$sth->execute(array(':menuid'=>$menu_id));
			return $sth->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOExeption $e) {
			return $e;
		}
	}
	public function getItemLabel($var = 0){
		if(is_numeric($var)){
			$sth = $this->db->prepare("SELECT label FROM menu_items WHERE item_id=:itemid ORDER BY `order`");
			$sth->execute(array(':itemid'=>$var));
			$res = $sth->fetch();
			return $res['label'];
		} else {
			$sth = $this->db->prepare("SELECT label FROM menu_items WHERE path=:path ORDER BY `order` ASC");
			$sth->execute(array(':path'=>$var));
			$res = $sth->fetch();
			return $res['label'];
		}
	}
	
}
