<?php 

class Index_Controller_Index extends Controller{
  
  function __construct() {
    parent::__construct();
    $this->view->module = 'Index';
  }
  
  public function Index(){
    $this->view->render('index/index');
  }
}