<?php

class User_Model_Role extends Model{

  public function __construct(){
  	parent::__construct();
  }
  
  public function getUserRole($userid){
    $sth = $this->db->prepare("SELECT * FROM user_roles WHERE userid=:userid");
    $sth->execute(array(':userid'=>$userid));
    return $sth->fetchAll();
  }
  
  public function setUserRole($userid,$roleid){
    print_r(array($userid,$roleid));
    $sth = $this->db->prepare("SELECT * FROM user_roles WHERE user_id=:userid");
    $sth->execute(array(':userid'=>$userid));
    if($sth->rowCount() > 0){
      $uth = $this->db->prepare("UPDATE user_roles SET role_id=:roleid WHERE user_id=:userid");
      $uth->execute(array(':roleid'=>$roleid,':userid'=>$userid));
    } else {
      $ith = $this->db->prepare("INSERT INTO user_roles (user_id,role_id) VALUES(:userid,:roleid)");
      $ith->execute(array(':roleid'=>$roleid,':userid'=>$userid));
  	}	
  }
  
  public function getRole($roleid){
    $sth = $this->db->prepare("SELECT * FROM roles WHERE role_id=:roleid");
    $sth->execute(array(':roleid'=>$roleid));
    return $sth->fetch();
  }
  
  public function createRole($data){
    $sth = $this->db->prepare("INSERT INTO roles (role_name,seniority) VALUES(:role_name,:seniority)");
    $sth->execute(array(':role_name'=>$data['role_name'],':seniority'=>$data['seniority']));
    return $this->db->lastInsertId();
  }
  
  public function UpdateRole($roleid,$data){
    unset($data['roleid']);
    $data_arr[':roleid'] = $roleid;
    foreach($data as $key=>$value){
      $data_arr[':'.$key] = $value;
      $sql_arr[] = $key.'=:'.$key;
    }
    $uth = $this->db->prepare("UPDATE roles SET ".implode(',',$sql_arr).' WHERE role_id=:roleid');
    $uth->execute($data_arr);
  }
  
  public function deleteRole($roleid){
    $sth = $this->db->prepare("DELETE FROM roles WHERE role_id=:roleid");
    $sth->execute(array(':roleid'=>$roleid));			
  }
  
  public function getRoles(){
    $sth = $this->db->prepare("SELECT * FROM roles");
    $sth->execute();
    return $sth->fetchAll();
  }
}
