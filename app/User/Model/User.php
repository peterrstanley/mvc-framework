<?php

class User_Model_User extends Model{
  protected $user = '';

  function __construct() {
    parent::__construct();
    $this->salt = SALT;
    if(isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0)
      $this->user = $_SESSION['user_id'];
  }
  
  public function isLoggedIn(){
    if(isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0)
      return true;
    return false;
  }

  public function loginUser($username,$password){
    try{
      $login = $this->db->prepare("SELECT user_id FROM users WHERE username=:user AND password=:pass");
      $login->execute(array(':user'=>$username,':pass'=>$this->_generate_password($password)));   
    } catch (PDOException $e) {
      echo $e;
    }
    if($login->rowCount() == 1){
      $res = $login->fetch(PDO::FETCH_ASSOC);
      $_SESSION['user_id'] = $res['user_id'];
      $this->user = $res['user_id'];
      return true;
    } else {
      return false;
    }
  }
  
  public function logoutUser(){
    $this->user = '';
    unset($_SESSION['user_id']);
  }
  
  public function createUser($username,$password,$email){
    try {
      $reg = $this->db->prepare("INSERT INTO users (username,password,email) VALUES(:user,:pass,:email)");
      $reg->execute(array(':user'=>$username,':pass'=>$this->_generate_password($password),':email'=>$email));
    } catch (PDOException $e) {
      return false;
    }
    return true;
  }
  
  public function updateUser($userid = 0,$data){
    try {
    } catch (PDOException $e) {
      return false;
    }
    return true;
  }
  
  public function deleteUser($userid = 0){
    try {
      $del = $this->db->prepare("DELETE FROM users WHERE user_id=:userid");
      $del->execute(array(':userid'=>$userid));
    } catch (PDOException $e) {
      return false;
    }
    return true;
  }
  
  public function getUsers(){
    $sth = $this->db->prepare("SELECT * FROM users");
    $sth->execute();
    return $sth->fetchAll();
  }

  public function getUser($userid = 0){
    $sth = $this->db->prepare("SELECT * FROM users WHERE user_id=:userid");
    $sth->execute(array(':userid'=>$this->user));
    return $sth->fetch();
  }

  public function getUserId(){
    return $this->user;
  }

  public function setUserPassword($userid,$password){
    try {
      $uth = $this->db->prepare("UPDATE users SET password=:password WHERE user_id=:userid");
      $uth->execute(array(':userid'=>$userid,':password'=>$this->_generate_password($password)));
    } catch (PDOException $e) {
    	echo $e;
      return false;
    }
    return true;
  }

  protected function _generate_password($password){
    return sha1($this->salt.$password);
  }
}
