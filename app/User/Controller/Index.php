<?php

class User_Controller_Index extends Controller{
  
  public function __construct(){
    parent::__construct();
    $this->view->module = 'User';
  }
  
  public function Index(){
    $user = $this->model->getModel('user/user');
    if($user->isLoggedIn()){
      header("Location:".BASE_DIR."user/dashboard");
      exit;
    } else {
      header("Location:".BASE_DIR."user/login");
      exit;
    }
  }
  
  public function Login(){
    $this->view->render('user/login');
  }
  
  public function Logout(){
    $user = $this->model->getModel('user/user');
    $user->logoutUser();
    header("Location:".BASE_DIR."/user/login");
    exit;
  }
  
  public function loginPost(){
    $login = $this->model->getModel('user/user');
    $user = $_POST['username'];
    $pass = $_POST['password'];
    $res = $login->loginUser($user,$pass);
    if($res){
      header("Location:".BASE_DIR."/user/dashboard");
      exit;
    } else {
      $this->ErrorHandler->addError("User/Password invalid.");
      header("Location:".BASE_DIR."/user/login");
      exit;
    }
  }
  
  public function Register(){
    $this->view->render('user/register');
  }
  
  public function registerPost(){
    $register = $this->model->getModel('user/user');
    $user = $_POST['username'];
    $email = $_POST['email'];		
    $pass = $_POST['password'];
    $pass2 = $_POST['password2'];

    $chk1 = $this->db->prepare("SELECT * FROM users WHERE username=:username");
    $chk1->execute(array(':username'=>$user));
    $_SESSION['user']['post_val'] = $_POST;
    if($chk1->RowCount() > 0){
      $this->ErrorHandler->addError("Username already exists.");
      header("Location:".BASE_DIR."/user/register");
      exit;
    }
  
    $chk2 = $this->db->prepare("SELECT * FROM users WHERE email=:email");
    $chk2->execute(array(':email'=>$email));
    if($chk2->RowCount() > 0){
      $this->ErrorHandler->addError("Email has an account registered.",'info');
      header("Location:/".BASE_DIR."/user/register");
      exit;
    }
    if(strlen($user) < 1){
      $this->ErrorHandler->addError("A username is required for registration.",'warning');
      header("Location:/".BASE_DIR."/user/register");
      exit;
    }
    if(strlen($email) < 1){
      $this->ErrorHandler->addError("Email is required for registration.");
      header("Location:/".BASE_DIR."/user/register");
      exit;
    }
    if($pass !== $pass2){
      $this->ErrorHandler->addError("Passwords do not match.");
      header("Location:/".BASE_DIR."/user/register");
      exit;
    }
    if(strlen($pass) < 1){
      $this->ErrorHandler->addError("Password is required for registration.");
      header("Location:/".BASE_DIR."/user/register");
      exit;
    }
    $res = $register->createUser($user,$pass,$email);
    if($res === true){
      header("Location:/".BASE_DIR."/user/login");
      exit;
    } else {
      $this->ErrorHandler->addError("Had issues registering your account.",'warning');
      header("Location:/".BASE_DIR."/user/register");
      exit;
    }
  }
  
  public function Dashboard(){
    $user = $this->model->getModel('user/user');
    if($user->isLoggedIn()){
      $this->view->render('user/index');
    } else {
      $this->ErrorHandler->addError("You must be logged in to access this page",'warning');
      header("Location:/".BASE_DIR."/user/login");
      exit;
    }
  }
  
  public function profile($userid = 0){
    $u = $this->model->getModel('user/user');
    $user = ($userid > 0 && is_numeric($userid)) ? $userid : $u->getUserId();
    $this->view->model = $u;
    $this->view->userid = $user;
    $this->view->render('user/profile');
  }
  
  public function profileUpdate(){
    $u = $this->model->getModel('user/user');
    $role = $this->model->getModel('user/role');
    switch($_POST['category']){
      case "user":
        if($_POST['password'] !== $_POST['password2']){
          // Password does not match
          die('password not match');
        } elseif (strlen($_POST['password']) < 1) {
          // Password was not inserted
          die('password not inserted');
        } else {
          print_r(array($_POST['userid'],$_POST['password']));
          $u->setUserPassword($_POST['userid'],$_POST['password']);
        }
        break;
      case "role":
        $role->setUserRole($_POST['userid'],$_POST['roleid']);
        break;
      default:
        break;
    }
    header("Location: /".BASE_DIR."/user/profile/".$_POST['userid']);
    exit;
  }
  
  public function roles(){
    $this->view->model = $this->model->getModel('user/role');
    $this->view->render('user/role/index');
  }
  
  public function roleEdit($roleid = ''){
    if(strtolower($roleid) === "new" || $roleid === '')
      $roleid = 'new';
    $this->view->roleid = $roleid;
    $this->view->model = $this->model->getModel('user/role');
    $this->view->render('user/role/edit');
  }
  
  public function roleDelete($roleid){
    $role = $this->model->getModel('user/role');
    $role->deleteRole($roleid);
    header("Location: /".BASE_DIR."/user/roles");
    exit;
  }
  
  public function roleEditPost(){
    $role = $this->model->getModel('user/role');
    if($_POST['roleid'] === "new"){
      $role->createRole($_POST);
      header("Location: /".BASE_DIR."/user/roles");
    } else {
      $role->updateRole($_POST['roleid'],$_POST);
      header("Location: /".BASE_DIR."/user/roles");
      exit;
    }
  }
}
