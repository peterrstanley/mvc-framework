<?php

class Database extends PDO{
	
  public function __construct(){
    $dsn = "mysql:dbname=".DB_NAME.";host=".DB_HOST;
    parent::__construct($dsn,DB_USER,DB_PASS);
  }
}
