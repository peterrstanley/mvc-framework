<?php

/**
 * Basic Model Class
 * 
 * This class is the core Model class that all modules (located in the app 
 * folder) extends from to get all the basic core functionality. 
 * 
 * @package Libs
 * @author Peter R Stanley <peterrstanley@gmail.com>
 * @version v0.1.0
 * @copyright (c) 2013, Peter R Stanley
 * 
 */
class Model {
  public function __construct() {
    try{
      $this->db = new Database();
    } catch (PDOException $e){
      echo $e;
    }
    return true;
  }
  
  /**
   * Returns the Model Class
   * 
   * This function returns the class model.
   * 
   * @param string $path Module path to load the proper model file
   * @return \class 
   */
  public function getModel($path){
    $url = $this->_parseModelPath($path);  
    require_once APP_PATH.$url.'.php';
    $class = str_replace('/','_',$url);
    //$this->class = $class;
    return new $class();
  }
  
  /**
   * Parses Model Path
   * 
   * This function parses the path it receives and returns the proper directory
   * and class path.
   * 
   * 
   * @param string $path Module path to load the proper model file
   * @return string Directory/Class path
   */
  protected function _parseModelPath($path){
    $temp = explode('/',$path);
    $temp2[] = ucwords($temp[0]);
    $temp2[] = 'Model';
    for($i = 1; $i < count($temp);$i++){
      $temp2[] = ucwords($temp[$i]);
    }
    return implode('/',$temp2);
  }
  
  /**
   * List of Attribute Backend Types
   * 
   * This returns the backend types available for model attributes. These are used
   * to determine how the attribute data is properly stored in the database.
   * 
   * @return array List of Attribute Backend Types
   */
  public function getEntityBackendTypes(){
    return array('bool','decimal','int','options','text','varchar','static');
  }
  
  /**
   * List of Attribute Frontend Types
   * 
   * This returns the frontend types available for model attributes. These are
   * used to determine how the attribute field will look like on the site for users
   * to fill out the data.
   * 
   * @return (string) List of Attribute Frontend Types
   */
  public function getEntityFrontendTypes(){
    return array('textfield','textarea','select','multiselect','checkbox','radio');
  }
}
