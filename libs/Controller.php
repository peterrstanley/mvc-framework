<?php
/**
 * Basic Controller Class
 * 
 * This class is the core Controller class that all modules (located in the app 
 * folder) extends from to get all the basic core functionality. 
 * 
 * @package Libs
 * @author Peter R Stanley <peterrstanley@gmail.com>
 * @version v0.1.0
 * @copyright (c) 2013, Peter R Stanley
 * 
 */
class Controller {
  protected $args = array();

  public function __construct() {
    try{
      $this->db = new Database();
    } catch (PDOException $e){
      echo $e;
    } 
    $this->ErrorHandler = new ErrorHandler();    
    $this->model = new Model();
    $this->view = new View();
    $this->view->controller = $this;
  }

  public function generatePath($path){
    return BASE_DIR.$path;
  }

  public function getPath($stripped = false){
    if($stripped === true)
      return '/'.rtrim($_GET['url'],'/');
    else
      return BASE_DIR.'/'.rtrim($_GET['url'],'/');
  }

  public function setArgs($args){
    if(is_array($args))
      $this->args = array_merge($this->args,$args);
    else
      $this->args[] = $args;
  }

  public function getArgs(){
    return $this->args;
  }

  public function redirectPath($path){
    header('Location:'.BASE_DIR.$path);
    exit;
  }

  public function getController($module){
    $file = APP_PATH.ucwords($module)."/Controller/Index.php";
    $cont = ucwords($module)."_Controller_Index";
    if(file_exists($file)){
      require_once $file;
      return new $cont();
    } 
    return false;
  }
}
