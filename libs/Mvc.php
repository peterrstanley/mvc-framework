<?php
/**
 * System MVC Class
 * 
 * This is the core class that handles the parsing of the rewrite from the system
 * to determine what controller to use. The default controller is for Index.
 * 
 * @package Libs
 * @author Peter R Stanley <peterrstanley@gmail.com>
 * @version v0.1.0
 * @copyright (c) 2013, Peter R Stanley
 * 
 */
class Mvc {
  protected $admin_path = '';
  protected $admin_link = false;

  public function __construct() {
    $this->errorHandler = new ErrorHandler();
    $this->controller = new Controller();
    $this->_parse_path();
    $this->_get_controller();
  }

  /**
   * Parses the URL Rewrite Path
   * 
   */
  protected function _parse_path(){
    $url = explode('/',rtrim(substr($_GET['url'],1),'/'));
    $this->_url = array();
    foreach($url as $key=>$value){
      if($key === 0 && strtolower($value) === 'admin'){
        $this->admin_path = 'Admin/';
        $this->admin_link = true;
      } else {
         $this->_url[] = ucwords(strtolower($value));
      }
    }
  }
  
  /**
   * Loads the proper Module Controller
   * 
   * @return boolean
   */
  protected function _get_controller(){
    if(count($this->_url) < 1 || $this->_url[0] == ''){
      $file = APP_PATH."Index/Controller/Index.php";
      $cont = 'Index_Controller_Index';
    } else {
      if($this->admin_link === true){
        $file = APP_PATH.$this->admin_path.$this->_url[0]."/Controller/".$this->_url[0].".php";
        $cont = "Admin_Controller_".$this->_url[0];
      } else {
        $file = APP_PATH.$this->admin_path.$this->_url[0]."/Controller/Index.php";
        $cont = $this->_url[0]."_Controller_Index";
      }
      //die($file);
      if(!file_exists($file)){
        $this->errorHandler->addError('Invalid Controller');
        $this->controller->redirectPath('/index');
      }
    }

    require_once $file;
    $controller = new $cont();
    if(count($this->_url) > 2){
      $temp = $this->_url;
      unset($temp[0]);
      unset($temp[1]);
      $controller->setArgs($temp);
    }
    if(isset($this->_url[1]) && $this->_url[1] != ''){
      if(!method_exists($controller,$this->_url[1])){
        $this->errorHandler->addError('Invalid Methods');
        $this->controller->redirectPath('/'.strtolower($this->admin_path.$this->_url[0]).'/index');
      }
      if(isset($this->_url[2]))
        $controller->{$this->_url[1]}($this->_url[2]);
      else 
        $controller->{$this->_url[1]}();
    } else {
      $controller->Index();
    }
  }

  public function getModel($name){
    $url = $this->_parse_path($name);
    $file = APP_PATH.$this->admin_path."$path[0]/Model";
    for($i=1; $i < count($path); $i++){
      $file.= "/".ucwords($path[$i]);
    }
    require_once $file.'.php';
    $class = str_replace('/','_',$url);
    return new $class();
  }
  
  public function renderBlock($name){
  $path = $this->_parse_path($name);
  $file = APP_PATH.$this->admin_path."$path[0]/Block";
  for($i=1; $i < count($path); $i++){
    $file.= "/".ucwords($path[$i]);
  }
  require_once $file.".phtml";
  }
}