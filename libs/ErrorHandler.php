<?php
/**
 * Basic ErrorHandler Class
 * 
 * This class is the core ErrorHandler class that all modules (located in the app 
 * folder) extends from to get all the basic core functionality. 
 * 
 * @package Libs
 * @author Peter R Stanley <peterrstanley@gmail.com>
 * @version v0.1.0
 * @copyright (c) 2013, Peter R Stanley
 * 
 */
class ErrorHandler {
	
  protected $errors = array();
  protected $err_class = array(
    1 => 'alert alert-success',
    2 => 'alert alert-info',
    3 => 'alert alert-warning',
    4 => 'alert alert-danger');
  protected $err_icon = array(
    1 => 'glyphicon glyphicon-ok',
    2 => 'glyphicon glyphicon-info-sign',
    3 => 'glyphicon glyphicon-warning-sign',
    4 => 'glyphicon glyphicon-exclamation-sign');

  function __construct() {
    if(isset($_SESSION['err_handle'])){
      $this->errors = $_SESSION['err_handle'];
    }
  }

  public function addError($msg,$type = 'error'){
    $this->errors[$this->_typeCode($type)][] = $msg;
    $_SESSION['err_handle'] = $this->errors;
  }
	
  public function clearAllErrors(){
    $this->errors = array();
    $_SESSION['err_handle'] = $this->errors;
  }
	
  public function clearErrorType($type = 'error'){
    $this->errors[$this->_typeCode($type)] = array();
    $_SESSION['err_handle'] = $this->errors;
  }
		
  public function displayAllErrors(){
    foreach($this->errors as $type=>$errors){
      foreach($errors as $err_msg){
        echo '<div class="'.$this->err_class[$type].'">';
        echo '<span class="'.$this->err_icon[$type].'"></span> ';
        echo $err_msg;
        echo '</div>';
      }
    }
    $this->clearAllErrors();
  }

  public function displayErrors($type = 'error'){
    foreach($this->errors[$this->_typeCode($type)] as $err_msg){
      echo '<div class="'.$this->err_class[$this->_typeCode($type)].'">';
      echo '<span class="'.$this->err_icon[$this->_typeCode($type)].'"></span> ';
      echo $err_msg;
      echo '</div>';
    }
    $this->clearErrorType($type);
  }

  public function generateMessage($msg,$type = 'info'){
    echo '<div class="'.$this->err_class[$this->_typeCode($type)].'">';
    echo '<span class="'.$this->err_icon[$this->_typeCode($type)].'"></span> ';
    echo $msg;
    echo '</div>';
  }
  
  protected function _typeCode($type){
    switch(strtolower($type)){
      case 'success':
        return 1;
        break;
      case 'info':
        return 2;
        break;
      case 'warning':
        return 3;
        break;
      default:
        return 4;
        break;
    }
  }
}
