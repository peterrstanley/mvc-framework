<?php
/**
 * Basic View Class
 * 
 * This class is the core View class that all modules (located in the app 
 * folder) extends from to get all the basic core functionality. 
 * 
 * @package Libs
 * @author Peter R Stanley <peterrstanley@gmail.com>
 * @version v0.1.0
 * @copyright (c) 2013, Peter R Stanley
 * 
 */
class View {
  public $title = 'Default';

  function __construct() {
    try{
      $this->db = new Database();
    } catch (PDOException $e){
      echo $e;
    }
    $this->model = new Model();
    $this->ErrorHandler = new ErrorHandler();
  }
  
  public function render($name){
    $path = $this->_parse_path($name);   
    if(strtolower($path[0]) === 'admin'){
      $file = "app/$path[0]/$path[1]/View";
      for($i=2; $i < count($path); $i++){
        $file.= "/".ucwords($path[$i]);
      }
    } else {
      $file = "app/$path[0]/View";
      for($i=1; $i < count($path); $i++){
        $file.= "/".ucwords($path[$i]);
      }
    }
    $this->getPageHead();
    $this->getPageHeader();
    require_once $file.".phtml";
    $this->getPageFooter();
  }

  public function renderBlock($name){
    $path = $this->_parse_path($name); 
    if(strtolower($path[0]) === 'admin'){
      $file = "app/$path[0]/$path[1]/Block";
      for($i=2; $i < count($path); $i++){
        $file.= "/".ucwords($path[$i]);
      }
    } else {
      $file = "app/$path[0]/Block";
      for($i=1; $i < count($path); $i++){
        $file.= "/".ucwords($path[$i]);
      }
    }
    require_once $file.".phtml";
  }

  protected function _parse_path($name){
    $temp = explode('/',$name);
    $array = array();
    foreach($temp as $value){
      $array[] = ucwords($value);
    }
    return $array;
  }
  
  public function getPageHead(){
    if(isset($this->module)){
      if(file_exists("app/".$this->module."/View/html/head.phtml")){
        require_once "app/".$this->module."/View/html/head.phtml";
        return false;
      }
    }
    require "templates/head.phtml";
  }
  public function getPageHeader(){
    if(isset($this->module)){
      if(file_exists("app/".$this->module."/View/html/header.phtml")){
        require_once "app/".$this->module."/View/html/header.phtml";
        return false;
      }
    }
    require_once "templates/header.phtml";
    $this->ErrorHandler->displayAllErrors();
  }

  public function getPageFooter(){
    if(isset($this->module)){
      if(file_exists("app/".$this->module."/View/html/footer.phtml")){
        require_once "app/".$this->module."/View/html/footer.phtml";
        return false;
      }
    }
    require_once "templates/footer.phtml";
  }
}
