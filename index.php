<?php
/**
 *  Rewrite Index File
 * 
 * This is the page that handles all the loading of the required files, global
 * constants and sends the information to the MVC class. 
 * 
 * @author Peter R Stanley <peterrstanley@gmail.com>
 * @version v0.1.0
 * @copyright (c) 2013, Peter R Stanley
 * 
 */
session_start();
define('BASE_PATH', dirname(realpath(__FILE__)) . '/');
define('APP_PATH', BASE_PATH . 'app/');

require BASE_PATH.'config/config.php';

if(defined('DEBUG_MODE') && DEBUG_MODE ===true){
  error_reporting(E_ALL);
	ini_set('display_errors',1);
	
}


require BASE_PATH.'libs/Database.php';
require BASE_PATH.'libs/Mvc.php';
require BASE_PATH.'libs/ErrorHandler.php';
require BASE_PATH.'libs/Controller.php';
require BASE_PATH.'libs/Model.php';
require BASE_PATH.'libs/View.php';

$Mvc = new Mvc();
