<?php
/***
	This is the configuration file for the MVC framework. All the major flags needed 
	for the framwork will b located here.

	@author: Peter R Stanley (peterrstanley@gmail.com)
	@version: 0.1.0
***/

	define('DB_HOST','localhost');			//	Database host address
  define('DB_USER','user123');				//	Database username
  define('DB_PASS','test1234');				//	Database password
  define('DB_NAME','taskmgr');				//	Database Name
	define('BASE_DIR','');							//	Framework Base Directory
  define('SALT','');									//	Encryption Salt
  define('DEBUG_MODE',true);					//	Debug Mode Flag
